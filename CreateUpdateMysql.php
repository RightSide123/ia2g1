<?php
//Update - Delete Data Using PHP, from: http://coursesweb.net/php-mysql/
$res ='';
$upd_html ='';
$del_html ='';

//Connect to mysql
$conn = new mysqli('localhost', 'root', '', 'tests');

//check connection and set to use utf8
if(mysqli_connect_errno()) exit('Error connect: '. mysqli_connect_error());
else $conn->query('SET character_set_client="utf8",character_set_connection="utf8",character_set_results="utf8";');


//if form submited
if(isset($_POST['sbmt']) && isset($_POST['id'])){
  //if request to update, make update with data from field
  if($_POST['sbmt'] =='Update'){
    $id = intval($_POST['id']);
    $url = $conn->real_escape_string($_POST['url']);
    $title = $conn->real_escape_string($_POST['title']);
    $description = $conn->real_escape_string($_POST['description']);
    if($conn->query("UPDATE sites SET url='$url', title='$title', description='$description' WHERE id=". $id)) $res .='Row updated<br>';
    else $res .='Error update: '. $conn->error;
  }
  else if($_POST['sbmt'] =='Delete'){
  //if request to delete, delete the row with received $id
    $id = intval($_POST['id']);
    if($conn->query("DELETE FROM sites WHERE id=". $id)) $res .='Row deleted<br>';
    else $res .='Error delete: '. $conn->error;
  }
}
else if(isset($_GET['id'])){
  $id = intval($_GET['id']);
  $upd_html .='<form method="post" action="'.$_SERVER["PHP_SELF"].'"><input type="hidden" name="id" value="'. $id .'">';

  //gets name and email associated to $id for update
  $resql = $conn->query("SELECT * from sites WHERE id=".$id);
  while($row = $resql->fetch_assoc()) {
    $upd_html .='URL: <input type="text" name="url" value="'. $row['url'] .'"><br>
Title: <input type="text" name="title" value="'. $row['title'] .'"><br>
Description: <input type="text" name="description" value="'. $row['description'] .'"><br>';
  }
  $upd_html .='<input type="submit" name="sbmt" value="Update"></form><br>';
}
else {
//sets list with links for update and options for delete
  $resql = $conn->query("SELECT * from sites");
  while($row = $resql->fetch_assoc()){
    $upd_html .='<li><a href="'. $_SERVER["PHP_SELF"] .'?id='. $row['id'] .'" title="'. $row['title'] .'">'. $row['title'] .'</a></li>';
    $del_html .='<option value="'. $row['id'] .'">'. $row['title'] .'</)>';
  }

  $upd_html ='<ul>'. $upd_html .'</ul>';  //finishes html with list
  $del_html ='<form method="post" action="'.$_SERVER["PHP_SELF"].'"><select name="id">'. $del_html .'</select><br>
  <input type="submit" name="sbmt" value="Delete"></form>';  //finishes html with list
}


$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
<title>Update - Delete Data Using PHP</title>
</head>
<body>
<h1>Update - Delete Data Using PHP</h1>
<?php echo $res; ?>
<h2>Update</h2>
<?php echo $upd_html; ?>
<h2>Delete</h2>
<?php echo $del_html; ?>
</body>
</html>
